jQuery(document).ready(function($){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
        }
    });    
   
    //----- Open model CREATE -----//
    jQuery('#btn-add').click(function () {
        jQuery('#ajaxProbandModel').html("Neuer Proband");
        jQuery('#btn-save').val("add");
        jQuery('#myForm').trigger("reset");
        jQuery('#formModal').modal('show');
    });    
    
    // EDIT
    $('body').on('click', '.edit', function() {
        var id = $(this).data('id');
        
        $.ajax({
            type: "POST",
            url: 'edit-subject',            
            data: {
                id: id
            },
            dataType: 'json',
            success: function(res) {
                $('#ajaxProbandModel').html("Proband bearbeiten");
                $('#formModal').modal('show');
                $('#id').val(res.id);
                $('#probandid').val(res.probandid);
                $('#lastname').val(res.lastname);
                $('#firstname').val(res.firstname);
                $('#yearofbirth').val(res.yearofbirth);
                $('#deployment').val(res.deployment);
                $('#drivinglicense').val(res.drivinglicense);
                $('#height').val(res.height);                
                $("#smoker").val(res.smoker).change();
                $("#righthanded").val(res.righthanded).change();
                $('#eyeglasses').val(res.eyeglasses);
                $('#weight').val(res.weight);
            }            
        });        
    });    
    
    // DELETE
    $('body').on('click', '.delete', function() {
        if (confirm("Delete Record?") === true) {
            var id = $(this).data('id');
                $.ajax({
                    type: "POST",
                    url: 'delete-subject',
                    data: {
                        id: id
                    },
                    dataType: 'json',
                    success: function(res) {
                        window.location.reload(true);
                    }
                });
            }
    });
    
    // CREATE
    $("#btn-save").click(function (e) {
        e.preventDefault();
        /*if (!$('#firstname').val()) {
            return alert('Bitte einen Vornamen eingeben.');
        }
        if (!$('#lastname').val()) {
            return alert('Bitte einen Nachnamen eingeben.');
        }*/
        if (!$('#probandid').val()) {
            return alert('Bitte eine Pfobanden-ID eingeben.');
        }
        if ($(document.activeElement).val() === "add") {
            var formData = {
                id: null,
                probandid: jQuery('#probandid').val(),
                lastname: jQuery('#lastname').val(),
                firstname: jQuery('#firstname').val(),
                yearofbirth: jQuery('#yearofbirth').val(),
                deployment: jQuery('#deployment').val(),
                drivinglicense: jQuery('#drivinglicense').val(),
                height: jQuery('#height').val(),
                smoker: jQuery('#smoker').val(),
                righthanded: jQuery('#righthanded').val(),
                eyeglasses: jQuery('#eyeglasses').val(),
                weight: jQuery('#weight').val()
            };
        } else {
            var formData = {
                id: jQuery('#id').val(),
                probandid: jQuery('#probandid').val(),
                lastname: jQuery('#lastname').val(),
                firstname: jQuery('#firstname').val(),
                yearofbirth: jQuery('#yearofbirth').val(),
                deployment: jQuery('#deployment').val(),
                drivinglicense: jQuery('#drivinglicense').val(),
                height: jQuery('#height').val(),
                smoker: jQuery('#smoker').val(),
                righthanded: jQuery('#righthanded').val(),
                eyeglasses: jQuery('#eyeglasses').val(),
                weight: jQuery('#weight').val()
            };
        };
        
        var type = "POST";
        var ajaxurl = 'add-update-subject';
        $.ajax({
            type: type,
            url: ajaxurl,
            data: formData,
            dataType: 'json',
            success: function(res) {
                window.location.reload(true);
            },
            error: function (res) {
                console.log(res);
            }
        });        
    });    
});

